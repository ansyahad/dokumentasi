# Angular module

## Prerequisite

Untuk mengikuti tutorial ini maka ada beberapa hal yang harus 
diinstall di dalam environment yaitu :
1. NodeJS
2. Angular CLI

untuk memastikan apakah kedua file sudah terinstall atau belum
dapat menggunakan script berikut pada terminal

- NodeJS

`$ npm -v`

dan hasilnya

`6.5.0-next.0`

Jika belum maka ketik pada terminal

    sudo apt-get update
    sudo apt-get install nodejs

kemudian ketik

```sudo apt-get install npm```

tunggu sampai instalasi selesai

- AngularCLI

`$ ng --version`

dan hasilnya

```

     _                      _                 ____ _     ___
    / \   _ __   __ _ _   _| | __ _ _ __     / ___| |   |_ _|
   / △ \ | '_ \ / _` | | | | |/ _` | '__|   | |   | |    | |
  / ___ \| | | | (_| | |_| | | (_| | |      | |___| |___ | |
 /_/   \_\_| |_|\__, |\__,_|_|\__,_|_|       \____|_____|___|
                |___/


Angular CLI: 7.2.2
Node: 11.6.0
OS: linux x64
Angular:

Package                      Version
------------------------------------------------------
@angular-devkit/architect    0.12.2
@angular-devkit/core         7.2.2
@angular-devkit/schematics   7.2.2
@schematics/angular          7.2.2
@schematics/update           0.12.2
rxjs                         6.3.3
typescript                   3.2.2
```

Jika belum terinstall maka ketikan pada terminal

`sudo npm install -g @angular/cli@latest`

tunggu sampai instalasi selesai

## Overview

Angular module (Ng module) adalah wadah untuk blok kode kohesif yang didedikasikan untuk domain aplikasi, workflow, atau serangkaian fitur lain yang terkait erat. Mereka dapat berisi komponen, service provider, dan file kode lainnya yang ruang lingkupnya ditentukan oleh file yang berisikan NgModule. Mereka dapat mengimpor fungsionalitas yang diekspor dari NgModules lain, dan mengekspor fungsionalitas yang dipilih untuk digunakan oleh NgModules lain.

Setiap aplikasi angular memiliki setidaknya satu kelas Ngmodule, root module, yang biasanya dinamakan AppModule dan tersedia di dalam file bernama app.module.ts. Kita dapat meluncurkan aplikasi dengan melakukan bootstrap pada root NgModule.

## Membuat Angular Module

Jika kita menginisiasi sebuah project baru dengan angular maka secara otomatis akan dibuat sebuah root module yang bernama AppModule pada file yang bernama app.module.ts, yang berisi :
```
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```
pada root module terdapat beberapa properti penting seperti :

- declaration : Komponen, arahan, dan pipa yang termasuk dalam NgModule ini.

- export: Subset deklarasi yang harus terlihat dan dapat digunakan dalam template komponen NgModules lainnya.

- import: Modul lain yang kelas ekspornya diperlukan oleh template komponen yang dideklarasikan dalam NgModule ini.

- providers: Pembuat layanan yang disumbangkan oleh NgModule ini ke koleksi layanan global; mereka dapat diakses di semua bagian aplikasi. (Anda juga dapat menentukan penyedia di tingkat komponen, yang sering lebih disukai.)

- bootstrap: Tampilan aplikasi utama, yang disebut komponen root, yang menampung semua tampilan aplikasi lainnya. Hanya root NgModule yang harus mengatur properti bootstrap.

Sementara aplikasi kecil mungkin hanya memiliki satu NgModule, sebagian besar aplikasi memiliki lebih banyak modul fitur. Jika kita ingin membuat sebuah module baru maka dapat dilakukan pada terminal yang sudah terinstall Angular CLI dengan mengetikkan

`ng generate module [name]`

atau

`ng g m [name]`

contoh 

`ng generate module contoh-module`

maka output pada terminal adalah :

``CREATE src/app/contoh-module/contoh-module.module.ts (196 bytes)``

Kemudian pada folder src/app akan muncul folder baru bernama "contoh-module" yang berisi sebuah file typescript bernama "contoh-module.module.ts" dengan script default sebagai berikut :
```
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class ContohModuleModule { }
```