# Angular Service

## Prerequisite

Untuk mengikuti tutorial ini maka ada beberapa hal yang harus 
diinstall di dalam environment yaitu :
1. NodeJS
2. Angular CLI

untuk memastikan apakah kedua file sudah terinstall atau belum
dapat menggunakan script berikut pada terminal

- NodeJS

`$ npm -v`

dan hasilnya

`6.5.0-next.0`

Jika belum maka ketik pada terminal

    sudo apt-get update
    sudo apt-get install nodejs

kemudian ketik

```sudo apt-get install npm```

tunggu sampai instalasi selesai

- AngularCLI

`$ ng --version`

dan hasilnya

```

     _                      _                 ____ _     ___
    / \   _ __   __ _ _   _| | __ _ _ __     / ___| |   |_ _|
   / △ \ | '_ \ / _` | | | | |/ _` | '__|   | |   | |    | |
  / ___ \| | | | (_| | |_| | | (_| | |      | |___| |___ | |
 /_/   \_\_| |_|\__, |\__,_|_|\__,_|_|       \____|_____|___|
                |___/


Angular CLI: 7.2.2
Node: 11.6.0
OS: linux x64
Angular:

Package                      Version
------------------------------------------------------
@angular-devkit/architect    0.12.2
@angular-devkit/core         7.2.2
@angular-devkit/schematics   7.2.2
@schematics/angular          7.2.2
@schematics/update           0.12.2
rxjs                         6.3.3
typescript                   3.2.2
```

Jika belum terinstall maka ketikan pada terminal

`sudo npm install -g @angular/cli@latest`

tunggu sampai instalasi selesai

## Overview

Service adalah sebuah objek yang mencakup nilai, fungsi, atau fitur apa pun yang dibutuhkan aplikasi. Suatu layanan biasanya adalah sebuah kelas dengan tujuan yang spesifik dan terdefinisi dengan jelas.

Sebuah component tidak boleh mengambil atau menyimpan data secara langsung dan mereka tentu saja tidak boleh secara sengaja menyajikan data palsu. Mereka harus fokus pada penyajian data dan mendelegasikan akses data ke service.

Angular membedakan komponen dari layanan untuk meningkatkan modularitas dan usabilitas ulang. Dengan memisahkan fungsionalitas terkait tampilan suatu komponen dari jenis pemrosesan lainnya, Anda dapat membuat kelas komponen Anda lebih ramping dan efisien.

Idealnya, pekerjaan komponen adalah untuk mengaktifkan pengalaman pengguna dan tidak lebih. Suatu komponen harus menyajikan properti dan metode untuk pengikatan data, untuk memediasi antara tampilan (diberikan oleh templat) dan logika aplikasi (yang sering menyertakan beberapa gagasan tentang model).

Komponen dapat mendelegasikan tugas tertentu ke layanan, seperti mengambil data dari server, memvalidasi input pengguna, atau masuk langsung ke konsol. Dengan mendefinisikan tugas pemrosesan seperti itu di kelas layanan injection Anda membuat tugas-tugas itu tersedia untuk komponen apa pun. Anda juga dapat membuat aplikasi Anda lebih mudah beradaptasi dengan menyuntikkan provider service yang sama, yang sesuai dalam situasi yang berbeda.

## Cara membuat service

Pada terminal yang telah terpasang Angular CLI, ketikkan

`ng generate service [name]`

atau

`ng generate s [name]`

maka akan muncul output pada terminal eperti berikut :
```
CREATE src/app/contoh-service.service.spec.ts (369 bytes)
CREATE src/app/contoh-service.service.ts (142 bytes)
```

maka pada folder root akan muncul 2 file yaitu
1. contoh-service.service.ts 
2. contoh-service.service.spec.ts

yang berisi :

contoh-service.service.ts 

```
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ContohServiceService {

  constructor() { }
}
```
contoh-service.service.spec.ts

```

import { TestBed } from '@angular/core/testing';

import { ContohServiceService } from './contoh-service.service';

describe('ContohServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ContohServiceService = TestBed.get(ContohServiceService);
    expect(service).toBeTruthy();
  });
});
```
