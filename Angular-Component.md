# ANGULAR COMPONENT

## Prerequisite

Untuk mengikuti tutorial ini maka ada beberapa hal yang harus 
diinstall di dalam environment yaitu :
1. NodeJS
2. Angular CLI

untuk memastikan apakah kedua file sudah terinstall atau belum
dapat menggunakan script berikut pada terminal

- NodeJS

`$ npm -v`

dan hasilnya

`6.5.0-next.0`

Jika belum maka ketik pada terminal

    sudo apt-get update
    sudo apt-get install nodejs

kemudian ketik

```sudo apt-get install npm```

tunggu sampai instalasi selesai

- AngularCLI

`$ ng --version`

dan hasilnya

```

     _                      _                 ____ _     ___
    / \   _ __   __ _ _   _| | __ _ _ __     / ___| |   |_ _|
   / △ \ | '_ \ / _` | | | | |/ _` | '__|   | |   | |    | |
  / ___ \| | | | (_| | |_| | | (_| | |      | |___| |___ | |
 /_/   \_\_| |_|\__, |\__,_|_|\__,_|_|       \____|_____|___|
                |___/


Angular CLI: 7.2.2
Node: 11.6.0
OS: linux x64
Angular:

Package                      Version
------------------------------------------------------
@angular-devkit/architect    0.12.2
@angular-devkit/core         7.2.2
@angular-devkit/schematics   7.2.2
@schematics/angular          7.2.2
@schematics/update           0.12.2
rxjs                         6.3.3
typescript                   3.2.2
```

Jika belum terinstall maka ketikan pada terminal

`sudo npm install -g @angular/cli@latest`

tunggu sampai instalasi selesai

## Overview

Angular Component atau sederhananya disebut Component adalah hal yang paling mendasar dari sebuah aplikasi angular. Component ibarat balok yang dapat disusun dan dikumpulkan sedemikian rupa hingga menjadi rumah (Aplikasi)

Pada dasarnya component adalah sebuah class yang berfungsi sebagai controller untuk user interface. Component memiliki 3 bagian utama :
1. TypeScript code
2. Template HTML
3. CSS Styles

## Cara membuat component

Pada terminal yang sudah terinstall Angular CLI, ketik
      
`ng generate component [nama]`

atau

`ng g c [nama]`

contoh :

`ng g c contoh-component`

outputnya adalah :

```CREATE src/app/contoh-component/contoh-component.component.css (0 bytes)
CREATE src/app/contoh-component/contoh-component.component.html (35 bytes)
CREATE src/app/contoh-component/contoh-component.component.spec.ts (692 bytes)
CREATE src/app/contoh-component/contoh-component.component.ts (308 bytes)
UPDATE src/app/app.module.ts (1384 bytes)
```

maka pada folder root aplikasi angular akan muncul 4 file yaitu :

1. contoh-component.component.css - merupakan file styles untuk component

pada default berisi kosong


2. contoh-component.component.html  — marupakan template dari component

pada default berisikan :
```
<p>
  contoh-component works!
</p>
```

3. contoh-component.component.spec.ts  — merupakan unit test untuk component

pada default berisikan :
```
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContohComponentComponent } from './contoh-component.component';

describe('ContohComponentComponent', () => {
  let component: ContohComponentComponent;
  let fixture: ComponentFixture<ContohComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContohComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContohComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
```

4. contoh-component.component.ts  — merupakan class dari component

pada default berisikan :
```
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contoh-component',
  templateUrl: './contoh-component.component.html',
  styleUrls: ['./contoh-component.component.css']
})
export class ContohComponentComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
```

- selector: Selektor CSS yang memberi tahu Angular untuk membuat dan menyisipkan instance komponen ini di mana pun ia menemukan tag yang sesuai di template HTML. Misalnya, jika HTML aplikasi berisi <app-contoh-component> </app-contoh-component>, maka Angular menyisipkan instance tampilan ContohComponentComponent di antara tag tersebut.

- templateUrl: Alamat relatif modul dari template HTML komponen. Atau, kita dapat menyediakan inline HTML template,sebagai nilai dari properti template. Template ini mendefinisikan tampilan host komponen.

- styleUrls : Merupakan alamat relatif modul dari file CSS yang digunakan untuk styling halaman
    
